package com.biskot.app.rest;

import com.biskot.domain.model.AbstractModel;
import com.biskot.domain.model.AddItemModel;
import com.biskot.domain.model.Cart;
import com.biskot.domain.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/carts")
@Slf4j
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping
    public ResponseEntity<String> sendInvitation()  {
        cartService.createCart();
        return new ResponseEntity<>("Cart initialized", HttpStatus.OK);
    }

    @GetMapping("/{cartId}")
    public HttpEntity<? extends AbstractModel>  getCart(@PathVariable("cartId") long cartId) {
        try {
            final Cart cart = cartService.getCart(cartId);
            return new ResponseEntity<>(cart, HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            AbstractModel error = com.biskot.domain.model.Error.builder().msg(ex.getMessage()).build();
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(path = "/{cartId}")
    public HttpEntity<String> updateOffer(@RequestBody AddItemModel dto, @PathVariable("cartId") long cartId) {
        try {
            cartService.addItemToCart(cartId, dto.getProductId(), dto.getQuantityToAdd());
            return new ResponseEntity<>("Item added", HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }catch (RuntimeException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }




}
