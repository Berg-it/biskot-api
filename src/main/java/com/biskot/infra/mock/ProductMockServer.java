package com.biskot.infra.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.IntStream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class ProductMockServer {

    private WireMockServer wireMockServer = new WireMockServer(9001);

    @Autowired
    ResourceLoader resourceLoader;

    @PostConstruct
    private void configureMockServer() throws IOException {

        Resource resource = resourceLoader.getResource("classpath:mocks");
        File file = resource.getFile();
        final String path = file.getPath();

        wireMockServer.start();

        IntStream.range(1, 5).forEach(productId -> {
            try {
                wireMockServer.stubFor(
                        get(urlPathEqualTo("/products/" + productId))
                                .willReturn(aResponse()
                                        .withStatus(200)
                                        .withHeader("Content-Type", "application/json")
                                        .withBody(Files.readString(Paths.get(path+"/product_"+productId+".json"))))
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }
}
