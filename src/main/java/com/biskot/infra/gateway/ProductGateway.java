package com.biskot.infra.gateway;

import com.biskot.domain.model.Product;
import com.biskot.domain.spi.ProductPort;
import com.biskot.infra.gateway.payload.ProductResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;

@Service
public class ProductGateway implements ProductPort {

    private final  RestTemplate restTempalte;

    public ProductGateway(RestTemplate restTempalte) {
        this.restTempalte = restTempalte;
    }

    public Product getProduct(long productId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ProductResponse> entity = new HttpEntity<>(headers);
        final ProductResponse body = restTempalte.exchange("http://localhost:9001/products/" + productId, HttpMethod.GET, entity, ProductResponse.class).getBody();
        if(Objects.isNull(body)){
            return null;
        }else {
            return fromProductResponseToDto(body);
        }
    }

    private Product fromProductResponseToDto(ProductResponse body) {
        return Product.builder()
                .id(body.getId())
                .label(body.getLabel())
                .quantityInStock(body.getQuantityInStock())
                .unitPrice(body.getUnitPrice())
                .build();
    }

}
