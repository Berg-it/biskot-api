package com.biskot.infra.repository.dao;

import com.biskot.infra.repository.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepsitory extends JpaRepository<CartEntity, Long> {
}
