package com.biskot.infra.repository.dao;

import com.biskot.domain.model.Cart;
import com.biskot.infra.repository.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository  extends JpaRepository<ItemEntity, Long> {
}
