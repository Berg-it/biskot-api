package com.biskot.infra.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartEntity {

    @Id
    @GeneratedValue
    private Long id;

    private double totalPrice;


    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemEntity> items;

}
