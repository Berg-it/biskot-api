package com.biskot.domain.service;

import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Item;
import com.biskot.domain.model.Product;
import com.biskot.infra.gateway.ProductGateway;
import com.biskot.infra.repository.dao.CartRepsitory;
import com.biskot.infra.repository.entity.CartEntity;
import com.biskot.infra.repository.entity.ItemEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CartServiceImpl implements CartService {

    public static final int MAX_BY_CART = 100;
    private final CartRepsitory cartRepsitory;
    private final ProductGateway productGateway;


    public CartServiceImpl(CartRepsitory cartRepsitory, ProductGateway productGateway) {
        this.cartRepsitory = cartRepsitory;
        this.productGateway = productGateway;
    }

    @Override
    @Transactional
    public void createCart() {
        final CartEntity build = CartEntity.builder().build();
        final CartEntity save = cartRepsitory.save(build);
        log.info("ID of add cart is: "+save.getId());
    }

    @Override
    public Cart getCart(long cartId) {
        final Optional<CartEntity> byId = cartRepsitory.findById(cartId);
        if(!byId.isPresent()){
            return null;
        }else {
            return fromEntityToDto(byId.get());
        }
    }

    /**
     *
     * @param cartEntity
     * @return
     */
    private Cart fromEntityToDto(CartEntity cartEntity) {
        final Cart response = Cart.builder().build();
        response.setId(cartEntity.getId());
        response.setTotalPrice(cartEntity.getTotalPrice());
        List<Item> items = new ArrayList<>();
        for(ItemEntity item: cartEntity.getItems()){
            Item itemBuild =Item.builder().build();
                itemBuild.setProductId(item.getProductId());
                itemBuild.setUnitPrice(item.getUnitPrice());
                itemBuild.setQuantity(item.getQuantity());
            items.add(itemBuild);
        }
        response.setItems(items);
        return response;
    }

    @Override
    public void addItemToCart(long cartId, long productId, int quantityToAdd) {
        final Optional<CartEntity> byId = cartRepsitory.findById(cartId);
        if(!byId.isPresent()){
            log.info("Cart not exist");
            throw new RuntimeException("Car id is not exist");
        }else {
            final Product product = productGateway.getProduct(productId);
            if(Objects.isNull(product)){
                log.info("Product not exist");
                throw new RuntimeException("Product id is not exist");
            }else {
                if(quantityToAdd > product.getQuantityInStock()){
                    log.info("Added quantity of a product should not exceed the stock availability");
                    throw new IllegalArgumentException("Business rules have not been respected");
                }else {
                    final CartEntity cartEntity = byId.get();
                    final List<ItemEntity> items = cartEntity.getItems();
                    verifyTotalPriceRule(items, product.getUnitPrice(), quantityToAdd);
                    verifyNumberOfProductRule(productId, items);
                    addAnewItem(productId, quantityToAdd, product, cartEntity);
                }
            }
        }
    }

    /**
     *
     * @param productId
     * @param quantityToAdd
     * @param product
     * @param cartEntity
     */
    @Transactional
    void addAnewItem(long productId, int quantityToAdd, Product product, CartEntity cartEntity) {
        final ItemEntity build = ItemEntity.builder().build();
        build.setQuantity(quantityToAdd);
        build.setUnitPrice(product.getUnitPrice());
        build.setProductId(productId);
        cartEntity.getItems().add(build);

        cartEntity.setTotalPrice(cartEntity.getTotalPrice() + (quantityToAdd * product.getUnitPrice()));

        cartRepsitory.save(cartEntity);
    }

    /**
     * A cart cannot contain more than 3 different products
     * @param productId
     * @param items
     */
    private void verifyNumberOfProductRule(long productId, List<ItemEntity> items) {
        final Map<Long, ItemEntity> ItemEntitMappedByProduct = items.stream().collect(Collectors.toMap(ItemEntity::getProductId, Function.identity()));
        if(ItemEntitMappedByProduct.keySet().size() == 3 && !ItemEntitMappedByProduct.containsKey(productId)){
            throw new IllegalArgumentException("Business rules have not been respected");
        }
    }

    /**
     * Total price of the cart should not exceed 100 euros
     * @param items
     * @param unitPrice
     * @param quantityToAdd
     */
    private void verifyTotalPriceRule(List<ItemEntity> items, double unitPrice, int quantityToAdd) {
        double total = 0;
        for(ItemEntity itemEntity: items){
            total = total + (itemEntity.getUnitPrice() * itemEntity.getQuantity());
        }
        total = total + (unitPrice*quantityToAdd);
        if(total> MAX_BY_CART){
            throw new IllegalArgumentException("Business rules have not been respected");
        }
    }
}
