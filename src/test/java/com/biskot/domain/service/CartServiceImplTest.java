package com.biskot.domain.service;

import com.biskot.infra.gateway.ProductGateway;
import com.biskot.infra.repository.dao.CartRepsitory;
import com.biskot.infra.repository.entity.CartEntity;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

class CartServiceImplTest {

    @Mock
    private CartRepsitory cartRepsitory;

    @Mock
    private ProductGateway productGateway;

    @InjectMocks
    private CartServiceImpl cartService;


    @BeforeEach()
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void should_throw_runtimeexception_when_cart_not_exist(){
        given(cartRepsitory.findById(eq(1L))).willReturn(null);
        assertThrows(RuntimeException.class, ()-> cartService.addItemToCart(1L, 1L, 2));
    }


    @Test
    public void should_throw_runtimeexception_when_product_not_exist(){
        Optional<CartEntity> byId = Optional.of(CartEntity.builder().id(2L).build());
        given(cartRepsitory.findById(eq(1L))).willReturn(byId);
        when(productGateway.getProduct(eq(1))).thenReturn(null);
        assertThrows(RuntimeException.class, ()-> cartService.addItemToCart(1L, 1L, 2));
    }





}